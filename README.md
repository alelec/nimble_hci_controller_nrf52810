<!--
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#  KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
-->

# Nimble HCI controller for nrf52810

## Overview

This project converts a Nordic Semi nrf52810 based module into a standards compliant 
Bluetooth HCI controller thanks to mynewt and the Nimble BLE stack

## Building

1. Download and install Apache Newt.

You will need to download the Apache Newt tool, as documented in the [Getting Started Guide](https://mynewt.apache.org/latest/get_started/index.html).

2. Download the Apache Mynewt Core package (executed from this project directory).

```no-highlight
    $ newt install
```

3. Build the blehci app for the sim platform using the "nrf52810_blehci" target
(executed from this project directory).

```no-highlight
    $ newt build nrf52810_blehci
```

The Apache Newt tool should indicate the location of the generated blehci
executable. 
