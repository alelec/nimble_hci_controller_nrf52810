newt build nrf52_boot
#newt build nrf52810_blehci
newt create-image -2 nrf52810_blehci 1.0.0
newt mfg create blehci_nrf52810 1.0.0

#newt load  nrf52_boot 
#newt load -v nrf52810_blehci

arm-none-eabi-objcopy --input-target=binary --output-target=elf32-little --set-start=0x8000 bin/targets/nrf52810_blehci/app/apps/blehci/blehci.img bin/targets/nrf52810_blehci/app/apps/blehci/blehci.img.elf
